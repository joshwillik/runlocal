const test = require('ava')
const nginx = require('./nginx.js')
let {comment, rule, block} = nginx

let expected = `# This is an example nginx config
user www www;
worker_processes 5; # this is a commented rule
events {
  worker_connections 1024;
}
http {
  include conf.d/mime.types;
  location ~ {
    fastcgi_pass 127.0.0.1:8888;
  }
}
`;

test('basic', t=>{
    let actual = nginx.render_conf([
        comment('This is an example nginx config'),
        rule('user', 'www www'),
        rule('worker_processes', 5, 'this is a commented rule'),
        block('events', [
            rule('worker_connections', 1024),
        ]),
        block('http', [
            rule('include', 'conf.d/mime.types'),
            block('location ~', [
                rule('fastcgi_pass', '127.0.0.1:8888'),
            ]),
        ]),
    ])
    t.is(actual, expected)
})
