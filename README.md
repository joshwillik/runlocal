# Runlocal

For some reason running local multi-service projects is annoying and hard.
I want it to be simple.

Yes I've tried using docker-compose. No, I don't don't want to mess around with kubernetes.

Yes, I realize that I'm basically building my own bad version of it

```
$ runlocal init
Built config file at .runlocal.conf

$ cat .runlocal.conf
[servers.www]
cmd = "npm run dev"
domain = "my-project.dev"
[servers.api]
cmd = "my-cool-init-script.sh"
domain = "api.myproject.dev"

$ runlocal
Building nginx config file
Generating fake nginx SSL keys
Starting www at 127.0.0.1:3000
Starting api at 127.0.0.1:3001
Starting cron server at 127.0.0.1:3002
Nginx listening on 127.0.0.1:80
Load https://my-project.dev in your browser to get started
```
