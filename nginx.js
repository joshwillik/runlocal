let E = module.exports

E.comment = comment=>({type: 'comment', comment})
E.rule = (name, value, comment)=>({type: 'rule', name, value, comment})
E.block = (header, children)=>({type: 'block', header, children})

E.render_conf = (conf, {indent=0, ...state}={})=>{
    if (Array.isArray(conf)) {
        return conf.map(block=>E.render_conf(block, {indent, ...state})+'\n')
            .join('')
    }
    if (conf.type=='rule') {
        let comment = conf.comment ? ` # ${conf.comment}` : '';
        return ' '.repeat(indent)+`${conf.name} ${conf.value};${comment}`
    }
    if (conf.type=='comment')
        return ' '.repeat(indent)+`# ${conf.comment}`
    if (conf.type=='block') {
        return [
            ' '.repeat(indent)+`${conf.header} {\n`,
            E.render_conf(conf.children||[], {indent: indent+2, ...state}),
            ' '.repeat(indent)+`}`,
        ].join('')
    }
    let whatisthis = conf
    try { conf = JSON.stringify(conf) }
    catch(e){ /* nothing to do here */ }
    throw new Error(`I don't know how to render this: `+whatisthis)
}
